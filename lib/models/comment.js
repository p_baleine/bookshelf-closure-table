var BaseModel = require('./base');

var Comment = BaseModel.extend({
  tableName: 'comments',
  idAttribute: 'comment_id',

  bug: function() {
    return this.belongsTo(require('./bug'), 'bug_id');
  },

  author: function() {
    return this.belongsTo(require('./account'), 'author');
  },

  descendant: function() {
    return this.belongsToMany(Comment, 'tree_paths', 'ancestor', 'descendant');
//      .query('where', 'comments.comment_id', '<>', this.id); // omit self
  },

  ancestor: function() {
    return this.belongsToMany(Comment, 'tree_paths', 'descendant', 'ancestor');
//      .query('where', 'comments.comment_id', '<>', this.id); // omit self
  }
});

module.exports = Comment;
