var BaseModel = require('./base');

var Account = BaseModel.extend({
  tableName: 'accounts',
  idAttribute: 'account_id',

  comments: function() {
    return this.hasMany(require('./comment'), 'bug_id');
  }
});

module.exports = Account;
