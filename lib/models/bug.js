var BaseModel = require('./base');

var Bug = BaseModel.extend({
  tableName: 'bugs',
  idAttribute: 'bug_id',

  comments: function() {
    return this.hasMany(require('./comment'), 'bug_id');
  }
});

module.exports = Bug;
