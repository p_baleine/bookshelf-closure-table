var Bookshelf = require('bookshelf');
var config = require('../../db/config').database;
var appBookshelf = Bookshelf.appBookshelf = Bookshelf.initialize(config);

var BaseModel = appBookshelf.Model.extend({
}, {
  findAll: function() {
    var coll = appBookshelf.Collection.forge([], { model: this });
    return coll.fetch.apply(coll, arguments);
  }
});

module.exports = BaseModel;
