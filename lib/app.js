var koa = require('koa');
var route = require('koa-route');
var common = require('koa-common');
var join = require('path').join;
var render = require('./render');
var app = module.exports = koa();

app.use(common.favicon());
app.use(common.logger());
app.use(common.static(join(__dirname, '../public')));

app.use(route.get('/', list));

function *list() {
  this.body = yield render('list');
}
