module.exports = {
  directory: './db/migrations',
  database: {
    client: 'pg',
    connection: {
      host: 'localhost',
      user: 'pbaleine',
      database: 'bookshelf-closure-table',
      charset: 'utf8'
    },
    debug: true
  }
};
