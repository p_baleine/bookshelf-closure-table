
function account(accounts, name) {
  var i, l;

  for (i = 0, l = accounts.length; i < l; i++)
    if (accounts[i].name === name) return accounts[i];

  throw new Error('not found');
}

exports.up = function(knex, Promise) {
  return knex('bugs').select()
    .then(function(bugs) {
      return bugs[0];
    })
    .then(function(bug) {
      return [bug, knex('accounts').whereIn('name', ['Fran', 'Ollie', 'Kukla']).select()];
    })
    .spread(function(bug, accounts) {
      var fran = account(accounts, 'Fran');
      var ollie = account(accounts, 'Ollie');
      var kukla = account(accounts, 'Kukla');

      return [bug, fran, ollie, kukla];
    })
    .spread(function(bug, fran, ollie, kukla) {
      console.log('bug-----------------------------');
      console.log(bug);
      return knex('comments').insert([
        { bug_id: bug.bug_id, author: fran.account_id, comment: 'What\'s the cause of this bug?' },
        { bug_id: bug.bug_id, author: ollie.account_id, comment: 'I think it\'s a null pointer.' },
        { bug_id: bug.bug_id, author: fran.account_id, comment: 'No, I checked for that.' },
        { bug_id: bug.bug_id, author: kukla.account_id, comment: 'We need to check for valid input.' },
        { bug_id: bug.bug_id, author: ollie.account_id, comment: 'Yes, that\'s a bug.' },
        { bug_id: bug.bug_id, author: fran.account_id, comment: 'Yes, please add a check.' },
        { bug_id: bug.bug_id, author: kukla.account_id, comment: 'That fixed it.' }
      ]);
    })
    .then(function() {
      return knex('tree_paths').insert([
        { ancestor: 1, descendant: 1 },
        { ancestor: 1, descendant: 2 },
        { ancestor: 1, descendant: 3 },
        { ancestor: 1, descendant: 4 },
        { ancestor: 1, descendant: 5 },
        { ancestor: 1, descendant: 6 },
        { ancestor: 1, descendant: 7 },
        { ancestor: 2, descendant: 2 },
        { ancestor: 2, descendant: 3 },
        { ancestor: 3, descendant: 3 },
        { ancestor: 4, descendant: 4 },
        { ancestor: 4, descendant: 5 },
        { ancestor: 4, descendant: 6 },
        { ancestor: 5, descendant: 5 },
        { ancestor: 6, descendant: 6 },
        { ancestor: 6, descendant: 7 }
      ]);
    });
};

exports.down = function(knex, Promise) {
};
