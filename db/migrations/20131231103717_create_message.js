
exports.up = function(knex, Promise) {
  return Promise.all([
    knex.schema.createTable('accounts', function(t) {
      t.increments('account_id').primary();
      t.string('name').notNullable();
    }),
    knex.schema.createTable('bugs', function(t) {
      t.increments('bug_id').primary();
      t.string('title').notNullable();
    }),
    knex.schema.createTable('comments', function(t) {
      t.increments('comment_id').primary();
      t.integer('bug_id').notNullable().references('bug_id').inTable('bugs');
      t.integer('author').notNullable().references('account_id').inTable('accounts');
      t.text('comment').notNullable();
      t.timestamps();
    })
  ]).then(function() {
    return knex.schema.createTable('tree_paths', function(t) {
      t.integer('ancestor').notNullable().references('comment_id').inTable('comments');
      t.integer('descendant').notNullable().references('comment_id').inTable('comments');
      t.primary(['ancestor', 'descendant']);
    });
  }).then(function() {
    return Promise.all([
      knex('accounts').insert([{ name: 'Fran' }, { name: 'Ollie' }, { name: 'Kukla' }]),
      knex('bugs').insert({ title: 'Title is not displayed' })
    ]);
  });
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTable('comments'),
    knex.schema.dropTable('bugs'),
    knex.schema.dropTable('accounts')
  ]);
};
