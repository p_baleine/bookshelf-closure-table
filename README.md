```js
var Comment = require('./lib/models/comment');
new Comment({ comment_id: 1 }).fetch({ withRelated: ['descendant', 'descendant.descendant'] })
  .then(function(comment) {
    console.log(comment.toJSON()); })
  .catch(console.error);
```

